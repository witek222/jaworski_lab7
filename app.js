// HTML Elements
const statusDiv = document.querySelector('.status');
const resetDiv = document.querySelector('.reset');
const cellDivs = document.querySelectorAll('.game-cell');

// game constants
const xSymbol = '×';
const oSymbol = '○';

// game variables
let gameIsLive = true;
let xnastepne = true;


// functions
const letterToSymbol = (letter) => letter === 'x' ? xSymbol : oSymbol;

const handleWin = (letter) => {
  gameIsLive = false;
  if (letter === 'x') {
    statusDiv.innerHTML = `${letterToSymbol(letter)} Wygrał!`;
  } else {
    statusDiv.innerHTML = `<span>${letterToSymbol(letter)} Wygrał!</span>`;
  }
};

const checkGameStatus = () => {
  const kw1 = cellDivs[0].classList[1];
  const kw2 = cellDivs[1].classList[1];
  const kw3 = cellDivs[2].classList[1];
  const kw4 = cellDivs[3].classList[1];
  const kw5 = cellDivs[4].classList[1];
  const kw6 = cellDivs[5].classList[1];
  const kw7 = cellDivs[6].classList[1];
  const kw8 = cellDivs[7].classList[1];
  const kw9 = cellDivs[8].classList[1];

  // check winner
  if (kw1 && kw1 === kw2 && kw1 === kw3) {
    handleWin(kw1);
    cellDivs[0].classList.add('won');
    cellDivs[1].classList.add('won');
    cellDivs[2].classList.add('won');
  } else if (kw4 && kw4 === kw5 && kw4 === kw6) {
    handleWin(kw4);
    cellDivs[3].classList.add('won');
    cellDivs[4].classList.add('won');
    cellDivs[5].classList.add('won');
  } else if (kw7 && kw7 === kw8 && kw7 === kw9) {
    handleWin(kw7);
    cellDivs[6].classList.add('won');
    cellDivs[7].classList.add('won');
    cellDivs[8].classList.add('won');
  } else if (kw1 && kw1 === kw4 && kw1 === kw7) {
    handleWin(kw1);
    cellDivs[0].classList.add('won');
    cellDivs[3].classList.add('won');
    cellDivs[6].classList.add('won');
  } else if (kw2 && kw2 === kw5 && kw2 === kw8) {
    handleWin(kw2);
    cellDivs[1].classList.add('won');
    cellDivs[4].classList.add('won');
    cellDivs[7].classList.add('won');
  } else if (kw3 && kw3 === kw6 && kw6 === kw9) {
    handleWin(kw3);
    cellDivs[2].classList.add('won');
    cellDivs[5].classList.add('won');
    cellDivs[8].classList.add('won');
  } else if (kw1 && kw1 === kw5 && kw1 === kw9) {
    handleWin(kw1);
    cellDivs[0].classList.add('won');
    cellDivs[4].classList.add('won');
    cellDivs[8].classList.add('won');
  } else if (kw3 && kw3 === kw5 && kw3 === kw7) {
    handleWin(kw3);
    cellDivs[2].classList.add('won');
    cellDivs[4].classList.add('won');
    cellDivs[6].classList.add('won');
  } else if (kw1 && kw2 && kw3 && kw4 && kw5 && kw6 && kw7 && kw8 && kw9) {
    gameIsLive = false;
    statusDiv.innerHTML = 'Remis!';
  } else {
    xnastepne = !xnastepne;
    if (xnastepne) {
      statusDiv.innerHTML = `Następny ${xSymbol}`;
    } else {
      statusDiv.innerHTML = `<span>Następny ${oSymbol}</span>`;
    }
  }
};


// event Handlers
const handleReset = () => {
  xnastepne = true;
  statusDiv.innerHTML = `Następny ${xSymbol}`;
  for (const cellDiv of cellDivs) {
    cellDiv.classList.remove('x');
    cellDiv.classList.remove('o');
    cellDiv.classList.remove('won');
  }
  gameIsLive = true;
};

const handleCellClick = (e) => {
  const classList = e.target.classList;
//nie da sie klikac 2 razy
  if (!gameIsLive || classList[1] === 'x' || classList[1] === 'o') {
    return;
  }

  if (xnastepne) {
    classList.add('x');
    checkGameStatus();
  } else {
    classList.add('o');
    checkGameStatus();
  }
};


// click
resetDiv.addEventListener('click', handleReset);

for (const cellDiv of cellDivs) {
  cellDiv.addEventListener('click', handleCellClick)
}
